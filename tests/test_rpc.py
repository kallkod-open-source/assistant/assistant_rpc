import os.path

DIR = "./assistant_rpc/"

def test_file_age():
    """
    Generated files should be the same age or newer than source file
    """
    source = os.path.getmtime(DIR + "assistant.proto")
    pb2 = os.path.getmtime(DIR + "assistant_pb2.py")
    grpc = os.path.getmtime(DIR + "assistant_pb2_grpc.py")

    assert source <= pb2
    assert source <= grpc
