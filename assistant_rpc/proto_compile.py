import grpc_tools
from grpc_tools import protoc

"""Run compilation of python code from .proto file.
PLEASE run shell script proto_complile instead of this one!"""

path_to_the_grpc_module = grpc_tools.__path__[0]

protoc.main((
    '',
    '-I' + path_to_the_grpc_module + "/_proto/",
    '-I.',
    '--python_out=.',
    '--grpc_python_out=.',
    './assistant.proto',
))
