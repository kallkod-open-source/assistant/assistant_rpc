import pathlib
from setuptools import setup

HERE = pathlib.Path(__file__).parent
REQUIREMENTS = (HERE / "requirements.txt").read_text().splitlines()


if __name__ == "__main__":
    setup(
        name='assistant_rpc',
        version='0.2.0.dev1',
        py_modules=[
            'assistant_rpc.__init__',
            'assistant_rpc.assistant_pb2_grpc',
            'assistant_rpc.assistant_pb2',
            ],
        install_requires=REQUIREMENTS,
        python_requires='==3.8.10',
    )
