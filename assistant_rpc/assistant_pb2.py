# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: assistant.proto
"""Generated protocol buffer code."""
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from google.protobuf import empty_pb2 as google_dot_protobuf_dot_empty__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='assistant.proto',
  package='assistant',
  syntax='proto3',
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\x0f\x61ssistant.proto\x12\tassistant\x1a\x1bgoogle/protobuf/empty.proto\"\x14\n\x04User\x12\x0c\n\x04name\x18\x01 \x01(\t\"*\n\x08UserList\x12\x1e\n\x05users\x18\x01 \x03(\x0b\x32\x0f.assistant.User\"\xac\x01\n\tUserInput\x12\x0c\n\x04user\x18\x01 \x01(\t\x12\x0c\n\x04text\x18\x02 \x01(\t\x12\r\n\x05\x63ount\x18\x03 \x01(\x05\x12\x35\n\x0c\x66orcedAction\x18\x04 \x01(\x0e\x32\x1f.assistant.UserInput.UserAction\x12\x10\n\x08\x64\x65\x61\x64line\x18\x05 \x01(\t\"+\n\nUserAction\x12\x08\n\x04\x41UTO\x10\x00\x12\x08\n\x04READ\x10\x01\x12\t\n\x05WRITE\x10\x02\"M\n\tOutputRow\x12\x0c\n\x04\x64\x61te\x18\x01 \x01(\t\x12\x12\n\nmessage_id\x18\x02 \x01(\t\x12\x0c\n\x04text\x18\x03 \x01(\t\x12\x10\n\x08\x64\x65\x61\x64line\x18\x04 \x01(\t\"e\n\x0cServerOutput\x12\"\n\x04rows\x18\x01 \x03(\x0b\x32\x14.assistant.OutputRow\x12\x0e\n\x06status\x18\x02 \x01(\t\x12!\n\x06\x61\x63tion\x18\x03 \x01(\x0e\x32\x11.assistant.Action\"0\n\x11LatestMsgsRequest\x12\x0c\n\x04user\x18\x01 \x01(\t\x12\r\n\x05\x63ount\x18\x02 \x01(\x05\"9\n\x04Type\x12!\n\x06\x61\x63tion\x18\x01 \x01(\x0e\x32\x11.assistant.Action\x12\x0e\n\x06status\x18\x02 \x01(\t*\x1d\n\x06\x41\x63tion\x12\x08\n\x04READ\x10\x00\x12\t\n\x05WRITE\x10\x01\x32\xd3\x02\n\x0c\x41ssistServer\x12<\n\x0bGetUserList\x12\x16.google.protobuf.Empty\x1a\x13.assistant.UserList\"\x00\x12\x37\n\nCreateUser\x12\x0f.assistant.User\x1a\x16.google.protobuf.Empty\"\x00\x12\x43\n\x10ProcessUserInput\x12\x14.assistant.UserInput\x1a\x17.assistant.ServerOutput\"\x00\x12Q\n\x16LatestRecordedMessages\x12\x1c.assistant.LatestMsgsRequest\x1a\x17.assistant.ServerOutput\"\x00\x12\x34\n\tGetTypeQA\x12\x14.assistant.UserInput\x1a\x0f.assistant.Type\"\x00\x62\x06proto3'
  ,
  dependencies=[google_dot_protobuf_dot_empty__pb2.DESCRIPTOR,])

_ACTION = _descriptor.EnumDescriptor(
  name='Action',
  full_name='assistant.Action',
  filename=None,
  file=DESCRIPTOR,
  create_key=_descriptor._internal_create_key,
  values=[
    _descriptor.EnumValueDescriptor(
      name='READ', index=0, number=0,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
    _descriptor.EnumValueDescriptor(
      name='WRITE', index=1, number=1,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=591,
  serialized_end=620,
)
_sym_db.RegisterEnumDescriptor(_ACTION)

Action = enum_type_wrapper.EnumTypeWrapper(_ACTION)
READ = 0
WRITE = 1


_USERINPUT_USERACTION = _descriptor.EnumDescriptor(
  name='UserAction',
  full_name='assistant.UserInput.UserAction',
  filename=None,
  file=DESCRIPTOR,
  create_key=_descriptor._internal_create_key,
  values=[
    _descriptor.EnumValueDescriptor(
      name='AUTO', index=0, number=0,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
    _descriptor.EnumValueDescriptor(
      name='READ', index=1, number=1,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
    _descriptor.EnumValueDescriptor(
      name='WRITE', index=2, number=2,
      serialized_options=None,
      type=None,
      create_key=_descriptor._internal_create_key),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=255,
  serialized_end=298,
)
_sym_db.RegisterEnumDescriptor(_USERINPUT_USERACTION)


_USER = _descriptor.Descriptor(
  name='User',
  full_name='assistant.User',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='name', full_name='assistant.User.name', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=59,
  serialized_end=79,
)


_USERLIST = _descriptor.Descriptor(
  name='UserList',
  full_name='assistant.UserList',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='users', full_name='assistant.UserList.users', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=81,
  serialized_end=123,
)


_USERINPUT = _descriptor.Descriptor(
  name='UserInput',
  full_name='assistant.UserInput',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='user', full_name='assistant.UserInput.user', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='text', full_name='assistant.UserInput.text', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='count', full_name='assistant.UserInput.count', index=2,
      number=3, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='forcedAction', full_name='assistant.UserInput.forcedAction', index=3,
      number=4, type=14, cpp_type=8, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='deadline', full_name='assistant.UserInput.deadline', index=4,
      number=5, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
    _USERINPUT_USERACTION,
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=126,
  serialized_end=298,
)


_OUTPUTROW = _descriptor.Descriptor(
  name='OutputRow',
  full_name='assistant.OutputRow',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='date', full_name='assistant.OutputRow.date', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='message_id', full_name='assistant.OutputRow.message_id', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='text', full_name='assistant.OutputRow.text', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='deadline', full_name='assistant.OutputRow.deadline', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=300,
  serialized_end=377,
)


_SERVEROUTPUT = _descriptor.Descriptor(
  name='ServerOutput',
  full_name='assistant.ServerOutput',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='rows', full_name='assistant.ServerOutput.rows', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='status', full_name='assistant.ServerOutput.status', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='action', full_name='assistant.ServerOutput.action', index=2,
      number=3, type=14, cpp_type=8, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=379,
  serialized_end=480,
)


_LATESTMSGSREQUEST = _descriptor.Descriptor(
  name='LatestMsgsRequest',
  full_name='assistant.LatestMsgsRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='user', full_name='assistant.LatestMsgsRequest.user', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='count', full_name='assistant.LatestMsgsRequest.count', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=482,
  serialized_end=530,
)


_TYPE = _descriptor.Descriptor(
  name='Type',
  full_name='assistant.Type',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='action', full_name='assistant.Type.action', index=0,
      number=1, type=14, cpp_type=8, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='status', full_name='assistant.Type.status', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=532,
  serialized_end=589,
)

_USERLIST.fields_by_name['users'].message_type = _USER
_USERINPUT.fields_by_name['forcedAction'].enum_type = _USERINPUT_USERACTION
_USERINPUT_USERACTION.containing_type = _USERINPUT
_SERVEROUTPUT.fields_by_name['rows'].message_type = _OUTPUTROW
_SERVEROUTPUT.fields_by_name['action'].enum_type = _ACTION
_TYPE.fields_by_name['action'].enum_type = _ACTION
DESCRIPTOR.message_types_by_name['User'] = _USER
DESCRIPTOR.message_types_by_name['UserList'] = _USERLIST
DESCRIPTOR.message_types_by_name['UserInput'] = _USERINPUT
DESCRIPTOR.message_types_by_name['OutputRow'] = _OUTPUTROW
DESCRIPTOR.message_types_by_name['ServerOutput'] = _SERVEROUTPUT
DESCRIPTOR.message_types_by_name['LatestMsgsRequest'] = _LATESTMSGSREQUEST
DESCRIPTOR.message_types_by_name['Type'] = _TYPE
DESCRIPTOR.enum_types_by_name['Action'] = _ACTION
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

User = _reflection.GeneratedProtocolMessageType('User', (_message.Message,), {
  'DESCRIPTOR' : _USER,
  '__module__' : 'assistant_pb2'
  # @@protoc_insertion_point(class_scope:assistant.User)
  })
_sym_db.RegisterMessage(User)

UserList = _reflection.GeneratedProtocolMessageType('UserList', (_message.Message,), {
  'DESCRIPTOR' : _USERLIST,
  '__module__' : 'assistant_pb2'
  # @@protoc_insertion_point(class_scope:assistant.UserList)
  })
_sym_db.RegisterMessage(UserList)

UserInput = _reflection.GeneratedProtocolMessageType('UserInput', (_message.Message,), {
  'DESCRIPTOR' : _USERINPUT,
  '__module__' : 'assistant_pb2'
  # @@protoc_insertion_point(class_scope:assistant.UserInput)
  })
_sym_db.RegisterMessage(UserInput)

OutputRow = _reflection.GeneratedProtocolMessageType('OutputRow', (_message.Message,), {
  'DESCRIPTOR' : _OUTPUTROW,
  '__module__' : 'assistant_pb2'
  # @@protoc_insertion_point(class_scope:assistant.OutputRow)
  })
_sym_db.RegisterMessage(OutputRow)

ServerOutput = _reflection.GeneratedProtocolMessageType('ServerOutput', (_message.Message,), {
  'DESCRIPTOR' : _SERVEROUTPUT,
  '__module__' : 'assistant_pb2'
  # @@protoc_insertion_point(class_scope:assistant.ServerOutput)
  })
_sym_db.RegisterMessage(ServerOutput)

LatestMsgsRequest = _reflection.GeneratedProtocolMessageType('LatestMsgsRequest', (_message.Message,), {
  'DESCRIPTOR' : _LATESTMSGSREQUEST,
  '__module__' : 'assistant_pb2'
  # @@protoc_insertion_point(class_scope:assistant.LatestMsgsRequest)
  })
_sym_db.RegisterMessage(LatestMsgsRequest)

Type = _reflection.GeneratedProtocolMessageType('Type', (_message.Message,), {
  'DESCRIPTOR' : _TYPE,
  '__module__' : 'assistant_pb2'
  # @@protoc_insertion_point(class_scope:assistant.Type)
  })
_sym_db.RegisterMessage(Type)



_ASSISTSERVER = _descriptor.ServiceDescriptor(
  name='AssistServer',
  full_name='assistant.AssistServer',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_start=623,
  serialized_end=962,
  methods=[
  _descriptor.MethodDescriptor(
    name='GetUserList',
    full_name='assistant.AssistServer.GetUserList',
    index=0,
    containing_service=None,
    input_type=google_dot_protobuf_dot_empty__pb2._EMPTY,
    output_type=_USERLIST,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='CreateUser',
    full_name='assistant.AssistServer.CreateUser',
    index=1,
    containing_service=None,
    input_type=_USER,
    output_type=google_dot_protobuf_dot_empty__pb2._EMPTY,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='ProcessUserInput',
    full_name='assistant.AssistServer.ProcessUserInput',
    index=2,
    containing_service=None,
    input_type=_USERINPUT,
    output_type=_SERVEROUTPUT,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='LatestRecordedMessages',
    full_name='assistant.AssistServer.LatestRecordedMessages',
    index=3,
    containing_service=None,
    input_type=_LATESTMSGSREQUEST,
    output_type=_SERVEROUTPUT,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='GetTypeQA',
    full_name='assistant.AssistServer.GetTypeQA',
    index=4,
    containing_service=None,
    input_type=_USERINPUT,
    output_type=_TYPE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
])
_sym_db.RegisterServiceDescriptor(_ASSISTSERVER)

DESCRIPTOR.services_by_name['AssistServer'] = _ASSISTSERVER

# @@protoc_insertion_point(module_scope)
