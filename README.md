# How to install assistant-rpc

## For start, install [assistant](https://gitlab.com/kallkod-assistant/assistant) package

according to the instructions in the [README.md](https://gitlab.com/kallkod-assistant/assistant/-/blob/master/README.md) file of `assistant` package.

## Next steps:

```bash
git clone git@gitlab.com:kallkod-assistant/assistant-rpc.git  # clone current project repository
source ~/Work/virtualenvs/env/bin/activate  # activate existing virtual envoronment, where the 'assistant' package is installed
cd ~/Work/assistant-rpc/  # cd to this project's root folder
pip install -e .  # install Assistant package to virtual environment.
```

## Done
Now `assistant-rpc` package installed in editable state, and all the edits made to the `.py` files will be automatically included in the installed package.

## ! Important
- [ ] Use the name **assistant_rpc** with underscore, instead of **assistant-rpc**, to avoid import errors when importing this package.
- [ ] This package have only two dependencies: `grpcio` and `grpcio-tools`, which are installed with `assistant` package. So there is no need to install them again if you have installed latest version of assistant. (for versions numbers see [requirements.txt in assistant package](https://gitlab.com/kallkod-assistant/assistant/-/blob/master/assistant/interface_algorithm/requirements.txt) )

## Version numbers
Version numbers in `setup.py` and git tags should follow the same rules as `assistant` project.
